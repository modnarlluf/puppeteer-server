export default async function (part) {
    const buff = await part.toBuffer()
    if (part.mimetype.startsWith('image')) {
        const base64 = buff.toString('base64')
        part.value = `data:${part.mimetype};base64,${base64}`
    } else {
        part.value = buff.toString()
    }
}
