# Puppeteer Server

## Setup

```shell
yarn
```

## Run

```shell
node server.js --port=10000 [--host=localhost]
```

## Endpoints

### POST /pdf

Content-type: `multipart/form-data`

- `html <string>` : ***Required***
- `pdfOptions <json>` : Puppeteer PdfOptions [as documented](https://github.com/puppeteer/puppeteer/blob/main/docs/api/puppeteer.pdfoptions.md). This part is parsed to create a JSON object.
  - `path` and `timeout` options are ignored
  - Default options are: 
  ```json
  {
    "format": "A4",
    "printBackground": true  
  }
  ```
- `[file_key]`: You can upload file to be used on `html` template. In your html content, add `<##file_key##>` to dump file content here
  - Files content with mimetype `image/*` are base64 encoded, so it can be used on `<img>` tag.
  - All others mimetype are used as utf-8 content
    - Example for an image: `<img src="<##LOGO##>">` with an image uploaded with the `LOGO` key on form data.
    - Example for css style: `<style><##STYLES##></style>` with a css file uploaded with the `STYLES` key on form data.

## Usage

```shell
curl --location --request POST 'http://localhost:10000/pdf' \
     --form 'html="<style><##CSS##></style>
<h1>Hello World</h1>
<img src=\"<##LOGO##>\"/>"' \
--form 'LOGO=@"/path/to/logo.svg"' \
--form 'CSS=@"/path/to/style.css"' \
--form 'pdfOptions=@"/path/to/pdfOptions.json"'
```

## Stack

- puppeteer@19.5.2
- fastify@4.11.0
- minimist@1.2.7
