import Fastify from 'fastify'
import minimist from 'minimist'
import fastifyMultipart from '@fastify/multipart'
import PdfGenerator from './PdfGenerator.js'
import onFile from './onFile.js'
import loadFileContentInHtml from './loadFileContentInHtml.js'

const argv = minimist(process.argv.slice(2))

const port = argv?.port
if (!port) {
    console.error('Missing --port={port} argument')
    process.exit(1)
}

const host = argv?.host ?? 'localhost'
const pdfGenerator = new PdfGenerator()


const fastify = Fastify({
    logger: true,
})
fastify.register(fastifyMultipart, {attachFieldsToBody: true, onFile})


fastify.post('/pdf', async (req, res) => {
    const html = req.body?.html?.value
    if (!html) {
        return res.code(400).send({error: 'Missing html form-data key'})
    }

    const finalHtml = loadFileContentInHtml(html, req.body)

    const pdfOptions = req.body?.pdfOptions?.value ? JSON.parse(req.body?.pdfOptions?.value) : {}
    delete pdfOptions.path
    delete pdfOptions.timeout

    if (pdfOptions.headerTemplate) {
        pdfOptions.headerTemplate = loadFileContentInHtml(pdfOptions.headerTemplate, req.body)
    }

    if (pdfOptions.footerTemplate) {
        pdfOptions.footerTemplate = loadFileContentInHtml(pdfOptions.footerTemplate, req.body)
    }

    const pdfContent = await pdfGenerator.generatePdf(finalHtml, pdfOptions)

    return res.code(201).header('content-type', 'application/pdf').send(pdfContent)
})

const start = async (config) => {
    try {
        await pdfGenerator.init()
        await fastify.listen(config)
    } catch (err) {
        fastify.log.error(err)
        process.exit(2)
    }
}
start({port, host})
