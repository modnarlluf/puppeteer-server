import puppeteer from 'puppeteer'

class PdfGenerator {
    constructor() {
        this.browser = null
        this.pdfOptions = {
            format: 'A4',
            printBackground: true,
        }
    }

    async init() {
        this.browser = await puppeteer.launch()
    }

    async generatePdf(html, pdfOptions) {
        const page = await this.browser.newPage()
        const setContentCompleted = page.setContent(html)
        const options = Object.assign({}, this.pdfOptions, pdfOptions)

        await setContentCompleted

        return page.pdf(options)
    }
}

export default PdfGenerator
