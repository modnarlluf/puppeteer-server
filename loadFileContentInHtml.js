
const loadFileContentInHtml = (html, body) => {
    const reducer = (acc, key) => {
        const part = body[key]
        if (!part?.file || !part?.value) {
            return acc
        }
        const regex = new RegExp(`<##${key}##>`, 'g')
        return acc.replace(regex, part.value)
    }

    return Object.keys(body).reduce(reducer, html)
}

export default loadFileContentInHtml
